import React from 'react';
import ReactDOM from 'react-dom';
import Master from './components/master.js';

ReactDOM.render(<Master />, document.getElementById('app'));
