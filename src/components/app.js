import React from 'react';
import Square from './square';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      squares: Array(9).fill(null),
      xIsNext: true,
    };
  }
  rendersquare(i){
    return(
      <Square value = {this.state.squares[i]} space={() => this.handleclick(i)}/>
    )
  }
  handleclick(i){
    const squares = this.state.squares;
    squares[i] = this.state.xIsNext ? 'X' : 'O';

    this.setState({
          squares: squares,
          xIsNext: !this.state.xIsNext,
        });
  }
   render() {
      return (
         <div>
           <div className="board-row">
            {this.rendersquare(0)}
            {this.rendersquare(1)}
            {this.rendersquare(2)}
          </div>
          <div className="board-row">
            {this.rendersquare(3)}
            {this.rendersquare(4)}
            {this.rendersquare(5)}
          </div>
          <div className="board-row">
            {this.rendersquare(6)}
            {this.rendersquare(7)}
            {this.rendersquare(8)}
          </div>
         </div>
      );
   }
}

export default App;
