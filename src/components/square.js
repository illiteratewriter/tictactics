import React from 'react';

class Square extends React.Component {
  render(){
    return (
       <button className="redsquare" onClick={this.props.space}>
          {this.props.value}
       </button>
    );
  }
}

export default Square;
